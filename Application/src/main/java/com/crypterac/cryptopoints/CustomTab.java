package com.crypterac.cryptopoints;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;

/**
 * Created by peter on 09/04/18.
 */

public class CustomTab extends Fragment {

    private PointCounter points = new PointCounter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_custom, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        setUpButtonListeners();
    }

    void setUpButtonListeners() {
        View.OnClickListener numpadListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button numpadButton = (Button) v;
                String buttonText = numpadButton.getText().toString();
                appendPoints(buttonText);
            }
        };
        getView().findViewById(R.id.custom_tab_numpad_0).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_1).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_2).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_3).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_4).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_5).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_6).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_7).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_8).setOnClickListener(numpadListener);
        getView().findViewById(R.id.custom_tab_numpad_9).setOnClickListener(numpadListener);

        getView().findViewById(R.id.custom_tab_numpad_clr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearPoints();
            }
        });
        getView().findViewById(R.id.custom_tab_numpad_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delPoints();
            }
        });

        Button enter_button = getView().findViewById(R.id.enter_button);
        enter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int sendPosition = ((ToggleSwitch)
                        getView().findViewById(R.id.send_redeem_toggle)).getCheckedTogglePosition();
                int typePosition = ((ToggleSwitch)
                        getView().findViewById(R.id.ether_crypterac_toggle)).getCheckedTogglePosition();

                ((MainActivity)getActivity()).startTransaction(points.getAmount(), sendPosition == 0,
                        typePosition == 0);

            }
        });

        ToggleSwitch toggleSwitch = getView().findViewById(R.id.ether_crypterac_toggle);
        toggleSwitch.setCheckedTogglePosition(1);
        TextView typeTextView = getView().findViewById(R.id.transaction_type);
        typeTextView.setText("CRTC");
        toggleSwitch.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                updateScreen();
                TextView typeTextView = getView().findViewById(R.id.transaction_type);
                if (position == 0) {
                    typeTextView.setText("ETH");
                } else {
                    typeTextView.setText("CRTC");
                }
            }
        });
    }

    void clearPoints() {
        points.clear();
        this.updateScreen();
    }

    void delPoints() {
        points.pop();
        this.updateScreen();
    }

    void appendPoints(String amount) {
        points.push(amount);
        this.updateScreen();
    }

    void updateScreen() {
        int position = ((ToggleSwitch)
                getView().findViewById(R.id.ether_crypterac_toggle)).getCheckedTogglePosition();
        if (position == 0) {
            TextView tv = getView().findViewById(R.id.transaction_value);
            tv.setText(String.valueOf(Math.round(points.getAmount()) / 1000d));
        } else {
            TextView tv = getView().findViewById(R.id.transaction_value);
            tv.setText(String.valueOf(points.getAmount()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        points.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        points.clear();
    }
}
package com.crypterac.cryptopoints;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by peter on 19/04/18.
 */

public class CommonItemListAdapter extends ArrayAdapter<CommonItem> {

    private Context context;
    private int resourceId;
    private LayoutInflater inflater;

    public CommonItemListAdapter(Context context, int resourceId, List<CommonItem> list) {
        super(context, resourceId, list);
        this.context = context;
        this.resourceId = resourceId;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(resourceId, null);
        CommonItem item = getItem(position);

        TextView name = convertView.findViewById(R.id.common_items_list_row_item_name);
        name.setText(item.getName());
        TextView points = convertView.findViewById(R.id.common_items_list_row_points);
        points.setText(Integer.toString(item.getPoints()));

        return convertView;
    }
}

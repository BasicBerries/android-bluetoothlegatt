package com.crypterac.cryptopoints;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by peter on 09/04/18.
 */

public abstract class CommonItemsTab extends Fragment {
    private final static String TAG = CommonItemsTab.class.getName();

    private final static int REQUEST_CODE_ADD_ITEM = 1;
    private final static int REQUEST_CODE_EDIT_ITEM = 2;

    private ArrayList<CommonItem> commonItemList;
    private Context context;
    private CommonItemListAdapter adapter;
    private JSONObject commonItemJson;

    public abstract String getCommonItemFilename();
    public abstract boolean isSending();
    public abstract int getCommonItemListResourceId();
    public abstract int getCommonItemListAddButtonResourceId();
    public abstract int getTabLayoutResourceId();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();

        commonItemList = new ArrayList<CommonItem>();
        commonItemJson = loadJsonFile(getCommonItemFilename());
        if (commonItemJson != null) {
            Iterator iter = commonItemJson.keys();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                try {
                    int points = commonItemJson.getInt(key);
                    commonItemList.add(new CommonItem(key, points));
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to parse key=" + key + ": " + e.getMessage());
                    continue;
                }
            }
        }

        adapter = new CommonItemListAdapter(context, R.layout.common_items_list_row, commonItemList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getTabLayoutResourceId(), container, false);
        ListView listview = rootView.findViewById(getCommonItemListResourceId());
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonItem item = (CommonItem) parent.getItemAtPosition(position);
                ((MainActivity)getActivity()).startTransaction(item.getPoints(), isSending(), false);
            }
        });
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeItem(adapter.getItem(position));
                    }
                };

                CommonItemDeleteDialogFragment dialog = new CommonItemDeleteDialogFragment();
                dialog.setup(positiveListener, null);
                dialog.show(getFragmentManager().beginTransaction(), "CommonItemDeleteDialogFragment");
                return true;
            }
        });

        FloatingActionButton button = rootView.findViewById(getCommonItemListAddButtonResourceId());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditCommonItemActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_ITEM);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ADD_ITEM:
                if (resultCode == Activity.RESULT_OK) {
                    String name = data.getStringExtra(EditCommonItemActivity.RESULT_INTENT_NAME);
                    int points = data.getIntExtra(EditCommonItemActivity.RESULT_INTENT_POINTS, 0);
                    Log.d(TAG, "AddItem worked. name=" + name + ", points=" + points);
                    // If key already exists, first delete then add it to that location.
                    boolean existingItem = false;
                    for (int i = 0; i < adapter.getCount(); i++) {
                        CommonItem item = adapter.getItem(i);
                        if (item.getName().equals(name)) {
                            adapter.remove(item);
                            adapter.insert(new CommonItem(name, points), i);
                            existingItem = true;
                            break;
                        }
                    }
                    if (!existingItem) {
                        adapter.add(new CommonItem(name, points));
                    }
                    addToCommonItemFile(name, points);
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Log.d(TAG, "AddItem cancelled.");
                } else {
                    Log.e(TAG, "Unknown resultcode=" + resultCode);
                }
                break;
            case REQUEST_CODE_EDIT_ITEM:
                break;
            default:
                Log.e(TAG, "Unknown requestcode=" + requestCode);
                break;
        }
    }

    public void removeItem(CommonItem item) {
        adapter.remove(item);
        removeFromCommonItemFile(item.getName(), item.getPoints());
    }

    private JSONObject loadJsonFile(String filepath) {
        String fileContent = null;
        File file = null;
        FileInputStream is = null;
        try {
            file = new File(context.getFilesDir(), getCommonItemFilename());
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            Log.i(TAG, "JSON File not found, so no list: " + e.getMessage());
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e2) {
                    Log.e(TAG, "Failed to close JSON FileInputStream: " + e.getMessage());
                }
            }
            return new JSONObject();
        }

        try {
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            fileContent = new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, "Failed to read file: " + getCommonItemFilename() + ": " + e.getMessage());
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e2) {
                    Log.e(TAG, "Failed to close JSON FileInputStream: " + e.getMessage());
                }
            }
            return null;
        }

        try {
            return new JSONObject(fileContent);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to create JSONObject: " + e.getMessage());
            return null;
        }
    }

    private void removeFromCommonItemFile(String name, int points) {
        commonItemJson.remove(name);
        writeToCommonItemFile();
    }

    private void addToCommonItemFile(String name, int points) {
        try {
            commonItemJson.put(name, points);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to create intermediate JSONObject: " + e.getMessage());
            return;
        }

        writeToCommonItemFile();
    }

    private void writeToCommonItemFile() {
        try {
            File file = new File(context.getFilesDir(), getCommonItemFilename());
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(commonItemJson.toString());
            writer.close();
        } catch (IOException e) {
            Log.e(TAG, "Failed to write commonItemJson to file: " + e.getMessage());
        }
    }
}
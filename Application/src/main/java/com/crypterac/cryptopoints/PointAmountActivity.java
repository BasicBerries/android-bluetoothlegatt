package com.crypterac.cryptopoints;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by peter on 02/04/18.
 */

public class PointAmountActivity extends Activity {

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String EXTRAS_SENDING_POINTS = "SENDING_POINTS";

    private String mDeviceName;
    private String mDeviceAddress;
    private Activity mThisActivity = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.point_amount);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        boolean isSending = intent.getBooleanExtra(EXTRAS_SENDING_POINTS, false);

        TextView promptView = findViewById(R.id.how_many_points_prompt);
        String prompt = "How many points to ";
        if (isSending) {
            prompt += "send?";
        } else {
            prompt += "receive?";
        }
        promptView.setText(prompt);

        EditText editText = findViewById(R.id.point_amount);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                final Intent intent = new Intent(mThisActivity, DeviceControlActivity.class);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, mDeviceName);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_POINT_AMOUNT, Integer.parseInt(v.getText().toString()));
                startActivity(intent);
                return true;
            }
        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

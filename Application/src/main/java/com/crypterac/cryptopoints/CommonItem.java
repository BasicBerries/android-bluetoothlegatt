package com.crypterac.cryptopoints;

/**
 * Created by peter on 19/04/18.
 */

public class CommonItem {

    private String name;
    private int points;

    public CommonItem(String name, int points) {
        super();
        this.setName(name);
        this.setPoints(points);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return this.points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}

package com.crypterac.cryptopoints;

/**
 * Created by peter on 09/04/18.
 */

public class RedeemTab extends CommonItemsTab {
    private final static String TAG = RedeemTab.class.getName();

    public String getCommonItemFilename() {
        return "RedeemTabCommonItemList.json";
    }
    public boolean isSending() {
        return false;
    }
    public int getCommonItemListResourceId() {
        return R.id.redeem_common_item_list;
    }
    public int getCommonItemListAddButtonResourceId() {
        return R.id.redeem_common_item_list_add_button;
    }
    public int getTabLayoutResourceId() {
        return R.layout.tab_redeem;
    }
}
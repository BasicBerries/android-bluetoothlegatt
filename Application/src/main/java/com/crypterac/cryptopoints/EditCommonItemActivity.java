package com.crypterac.cryptopoints;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

/**
 * Created by peter on 20/04/18.
 */

public class EditCommonItemActivity extends AppCompatActivity {
    private static final String TAG = EditCommonItemActivity.class.getName();
    public static final String RESULT_INTENT_NAME = "name";
    public static final String RESULT_INTENT_POINTS = "points";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_common_item);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i(TAG, "Received BACK/HOME button press. Going back...");
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;
            case R.id.edit_common_item_submit:
                String name = ((EditText)findViewById(R.id.edit_common_item_name)).getText().toString();
                String pointsStr = ((EditText)findViewById(R.id.edit_common_item_points)).getText().toString();
                if (pointsStr.equals("") || name.equals("")) {
                    Log.i(TAG, "Submitted, but name or points invalid. name=" + name + ", points=" + pointsStr);
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                    return true;
                }
                int points = Integer.parseInt(pointsStr);

                Intent resultIntent = new Intent();
                resultIntent.putExtra(RESULT_INTENT_NAME, name);
                resultIntent.putExtra(RESULT_INTENT_POINTS, points);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

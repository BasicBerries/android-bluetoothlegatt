package com.crypterac.cryptopoints;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by peter on 09/04/18.
 */

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getName();

    private Context mContext = this;
    private BleScanner mScanner;
    private BluetoothDevice mDevice;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeService mBluetoothLeService;
    private LoadingFragment loadingFragment;
    private static final int REQUEST_ENABLE_BT = 1;
    private boolean mConnected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up tabs
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Send"));
        tabLayout.addTab(tabLayout.newTab().setText("Redeem"));
        tabLayout.addTab(tabLayout.newTab().setText("Custom"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        checkBluetoothEnabled();

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        loadingFragment = new LoadingFragment();
        ft.replace(R.id.fragment_container, loadingFragment);
        ft.commit();

        mScanner = new BleScanner("PiAdvertiserECE", mBluetoothAdapter, new BleScanner.ScanCallback() {
            @Override
            public void onDeviceFound(BluetoothDevice device) {
                Log.i(TAG, "Got device=" + device.getName());
                TextView tv = (TextView) loadingFragment.getView().findViewById(R.id.connect_text);
                tv.setText("Connected to device, getting services");

                mDevice = device;
                registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
                bindToBleService();
                if (mBluetoothLeService != null) {
                    final boolean result = mBluetoothLeService.connect(device.getAddress());
                    Log.d(TAG, "Connect request result=" + result);
                }
            }
        });

        final ViewPager viewPager = findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void bindToBleService() {
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDevice.getAddress());
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                closeLoadingFragment();
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                String data = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
                if (data != null) {
                    if (mBluetoothLeService != null) {
                        Log.d(TAG, "Received data=" + data);
                        mBluetoothLeService.onDataReceived(data);
                    } else {
                        Log.e(TAG, "Received data before initializing BleTransaction, data=" + data);
                    }
                }
            } else if (BluetoothLeService.DISPLAY_DATA_AVAILABLE.equals(action)) {

                String displayData = intent.getStringExtra(BluetoothLeService.DISPLAY_DATA);
                if (loadingFragment != null && loadingFragment.isVisible() && loadingFragment.getView() != null) {
                    TextView tv = loadingFragment.getView().findViewById(R.id.connect_text);
                    tv.setText(displayData);
//                    Button button = loadingFragment.getView().findViewById(R.id.cancel_button);
//                    button.setEnabled(false);
//                    button.setBackgroundResource(R.drawable.my_button_disabled);
                }

                boolean ended = false;
                int sound = R.raw.low_battery;
                if (displayData.startsWith("Transaction failed")) {
                    ended = true;
                }
                if (displayData.startsWith("Transaction complete")) {
                    ended = true;
                    sound = R.raw.nfc_success;
                }
                if (ended) {
                    makeSound(sound);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            closeLoadingFragment();
                        }
                    }, 1000);
                }
            }
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        mScanner.scanForDevice(true);
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDevice.getAddress());
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScanner.scanForDevice(false);
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }


    public void startTransaction(int points, boolean isSending, boolean isEther) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        loadingFragment = new LoadingFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(LoadingFragment.ARG_TRANSACTION, true);
        loadingFragment.setArguments(bundle);
        ft.replace(R.id.fragment_container, loadingFragment);
        ft.commit();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (loadingFragment != null && loadingFragment.isVisible() && loadingFragment.getView() != null) {
                    final TextView tv = (TextView) loadingFragment.getView().findViewById(R.id.connect_text);
                    tv.setText("Ready for card");
                    makeSound(R.raw.nfc_initiated);
                }
            }
        }, 2000);
        mBluetoothLeService.startTransaction(points, isSending, isEther);
    }

    public void closeLoadingFragment() {
        if (loadingFragment != null && loadingFragment.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(loadingFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void checkBluetoothEnabled() {
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // TODO: Not sure why you need to do both this and thing above.
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private void makeSound(int resid) {
        try {
            MediaPlayer ring= MediaPlayer.create(MainActivity.this, resid);
            ring.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.DISPLAY_DATA_AVAILABLE);
        return intentFilter;
    }
}

package com.crypterac.cryptopoints;

import android.util.Log;

/**
 * Created by peter on 12/04/18.
 */

public class PointCounter {
    private static String tag = "PointCounter";
    private String mPointCount;

    public PointCounter() {
        this("0");
    }

    public PointCounter(String count) {
        mPointCount = count;
    }

    public void push(String count) {
        Log.d(tag, "pushing amt=" + count);
        String newCount;
        if (mPointCount.equals("0")) {
            newCount = count;
        } else {
            newCount = mPointCount + count;
            if (checkIfInteger(newCount) < 0) {
                return;
            }
        }
        mPointCount = newCount;
        Log.d(tag, "after push(), new amt=" + mPointCount);
    }

    public void pop() {
        if (mPointCount.equals("0")) {
            // pass
        } else if (mPointCount.length() == 1) {
            mPointCount = "0";
        } else {
            mPointCount = mPointCount.substring(0, mPointCount.length() - 1);
        }
        Log.d(tag, "called pop(), new point count=" + mPointCount);
    }

    public void clear() {
        Log.d(tag, "clear points.");
        mPointCount = "0";
    }

    public int getAmount() {
        return checkIfInteger(mPointCount);
    }

    private int checkIfInteger(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            Log.e(tag, "Point string too long, point=" + mPointCount);
            return -1;
        }
    }
}
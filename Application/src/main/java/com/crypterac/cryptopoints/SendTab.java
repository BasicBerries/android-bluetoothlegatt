package com.crypterac.cryptopoints;

/**
 * Created by peter on 09/04/18.
 */

public class SendTab extends CommonItemsTab {
    private final static String TAG = SendTab.class.getName();

    public String getCommonItemFilename() {
        return "SendTabCommonItemList.json";
    }
    public boolean isSending() {
        return true;
    }
    public int getCommonItemListResourceId() {
        return R.id.send_common_item_list;
    }
    public int getCommonItemListAddButtonResourceId() {
        return R.id.send_common_item_list_add_button;
    }
    public int getTabLayoutResourceId() {
        return R.layout.tab_send;
    }
}
package com.crypterac.cryptopoints;

/**
 * Created by peter on 13/04/18.
 */

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HttpServer {
    private static final String TAG = HttpServer.class.getName();
//    private static final String ROOT_URL = "https://c8eb956c.ngrok.io";
    private static final String ROOT_URL = "http://crypterac.us-east-2.elasticbeanstalk.com";
    private static final String urlSendPublicAddress       = ROOT_URL + "/transactions/receive";
    private static final String urlSendPublicAddressOnly   = ROOT_URL + "/transactions/send";
    private static final String urlSendSignedData          = ROOT_URL + "/transactions/receive/complete";

    private Context mContext;

    public HttpServer(Context context) {
        mContext = context;
    }


    public interface HttpServerResponse {
        void onResponse(JSONObject response, Exception exception);
    }

    public void sendPublicAddress(final String pa,
                                  final int pointAmount,
                                  final String type,
                                  final HttpServerResponse httpServerResponse) {
        RequestQueue queue = Volley.newRequestQueue(mContext);

        // Request a string response from the provided URL.
        Map<String, String> params = new HashMap<String, String>();
        params.put("fromAddress", pa);
        params.put("amount", Integer.toString(pointAmount));
        params.put("type", type);

        JSONObject paramsJson = new JSONObject(params);

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "got a response on sendPublicAddress().");
                httpServerResponse.onResponse(response, null);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                httpServerResponse.onResponse(null, error);
            }
        };

        Log.d(TAG, "sending pa=" + pa + ", points=" + pointAmount + ", to url=" + urlSendPublicAddress);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,
                                                                urlSendPublicAddress,
                                                                paramsJson,
                                                                responseListener,
                                                                errorListener);

        queue.add(stringRequest);
    }

    public void sendPublicAddressOnly(String pa, int pointAmount, String type,
                                      final HttpServerResponse httpServerResponse) {
        RequestQueue queue = Volley.newRequestQueue(mContext);

        // Request a string response from the provided URL.
        Map<String, String> params = new HashMap<String, String>();
        params.put("toAddress", pa);
        params.put("amount", Integer.toString(pointAmount));
        params.put("type", type);

        JSONObject paramsJson = new JSONObject(params);

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "got a response on sendPublicAddressOnly().");
                httpServerResponse.onResponse(response, null);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                httpServerResponse.onResponse(null, error);
            }
        };

        Log.d(TAG, "sending pa=" + pa + ", points=" + pointAmount + ", to url=" + urlSendPublicAddressOnly);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,
                urlSendPublicAddressOnly,
                paramsJson,
                responseListener,
                errorListener);

        queue.add(stringRequest);
    }

    public void sendSignedData(String signed, String messageHash, String publicAddress,
                               JSONObject transactionDetails,
                               final HttpServerResponse httpServerResponse) {
        RequestQueue queue = Volley.newRequestQueue(mContext);

        // Request a string response from the provided URL.
        Map<String, String> params = new HashMap<String, String>();
        params.put("respData", signed);
        params.put("message", messageHash);
        params.put("fromAddress", publicAddress);

        JSONObject paramsJson = new JSONObject(params);
        // TODO: Maybe can add this to the put()'s, above.
        try {
            paramsJson.put("transactionDetails", transactionDetails);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to add new key-value to paramsJson. key=transactionDetails, value="
                        + transactionDetails);
        }

        //writeFinishedCharacteristic.setValue("0");
        //mBluetoothGatt.writeCharacteristic(writeFinishedCharacteristic);

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "got a response on sendSignedData().");
                httpServerResponse.onResponse(response, null);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                httpServerResponse.onResponse(null, error);
            }
        };

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST,
                                                                urlSendSignedData,
                                                                paramsJson, responseListener,
                                                                errorListener);

        queue.add(stringRequest);
    }
}
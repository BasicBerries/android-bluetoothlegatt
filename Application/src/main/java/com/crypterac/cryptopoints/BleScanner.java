package com.crypterac.cryptopoints;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.util.Log;

/**
 * Created by peter on 13/04/18.
 */

public class BleScanner {

    public interface ScanCallback {
        void onDeviceFound(BluetoothDevice device);
    }

    static private final String TAG = BleScanner.class.getName();

    private BluetoothAdapter mBluetoothAdapter;
    private boolean isScanning = false;
    private String mDeviceName;
    private ScanCallback mCallback;

    public BleScanner(String deviceName, BluetoothAdapter bluetoothAdapter, ScanCallback callback) {
        mDeviceName = deviceName;
        mCallback = callback;
        mBluetoothAdapter = bluetoothAdapter;
    }

    public void scanForDevice(boolean enable) {
        if (enable) {
            if (!isScanning) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    }
                }, 100000);
                isScanning = true;
                Log.d(TAG, "Start scanning.");
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
        } else {
            if (isScanning) {
                isScanning = false;
                Log.d(TAG, "Stop scanning.");
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    // String readerName = "PiAdvertiserECE";
                    // TODO: Potentially use BluetoothAdapter.getRemoteDevice() instead?

                    // Look for specific device, and automatically connect to it.
                    if (device != null && device.getName() != null) {
                        Log.d(TAG, "Found device=" + device.getName() + ", address=" + device.getAddress());
                        if (device.getName().equals(mDeviceName)) {
                            scanForDevice(false);
                            mCallback.onDeviceFound(device);
                        }
                    }
                }
            };
}

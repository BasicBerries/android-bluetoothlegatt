/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.crypterac.cryptopoints;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceScanActivity extends Activity {
    private String tag = "CryptoApp-DeviceScanActivity";
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mFoundDevice;
    private Handler mHandler;
    private enum BleScanState {NOT_SCANNING, SCANNING, CONNECTED};
    private BleScanState mBleScanState = BleScanState.NOT_SCANNING;
    private Activity mThisActivity = this;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listitem_device);
        getActionBar().setTitle(R.string.title_devices);
        mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Set click listeners on send/receive buttons.
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {
                if (mFoundDevice != null) {
                    boolean isSending = v.equals(findViewById(R.id.send_button));
                    final Intent intent = new Intent(mThisActivity, PointAmountActivity.class);
                    intent.putExtra(PointAmountActivity.EXTRAS_DEVICE_NAME, mFoundDevice.getName());
                    intent.putExtra(PointAmountActivity.EXTRAS_DEVICE_ADDRESS, mFoundDevice.getAddress());
                    intent.putExtra(PointAmountActivity.EXTRAS_SENDING_POINTS, isSending);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mThisActivity);
                    builder.setMessage(R.string.device_not_connected_message)
                           .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   Log.d(tag, "Exiting the NOT_READY dialog.");
                               }
                           })
                           .show();
                }
            }
        };
        final Button sendButton = findViewById(R.id.send_button);
        final Button receiveButton = findViewById(R.id.receive_button);
        sendButton.setOnClickListener(listener);
        receiveButton.setOnClickListener(listener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        switch (mBleScanState) {
            case NOT_SCANNING:
                menu.findItem(R.id.menu_connected).setVisible(false);
                menu.findItem(R.id.menu_stop).setVisible(false);
                menu.findItem(R.id.menu_scan).setVisible(true);
                menu.findItem(R.id.menu_refresh).setActionView(null);
                break;
            case SCANNING:
                menu.findItem(R.id.menu_stop).setVisible(true);
                menu.findItem(R.id.menu_connected).setVisible(false);
                menu.findItem(R.id.menu_scan).setVisible(false);
                menu.findItem(R.id.menu_refresh).setActionView(
                        R.layout.actionbar_indeterminate_progress);
                break;
            case CONNECTED:
                menu.findItem(R.id.menu_stop).setVisible(false);
                menu.findItem(R.id.menu_connected).setVisible(true);
                menu.findItem(R.id.menu_scan).setVisible(false);
                menu.findItem(R.id.menu_refresh).setActionView(null);
                break;
            default:
                Log.e(tag, "Shouldn't be here! Can't be scanning and found a device.");
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        // Initializes list view adapter.
        if (mBleScanState != BleScanState.CONNECTED) {
            scanLeDevice(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mBleScanState == BleScanState.SCANNING) {
                        mBleScanState = BleScanState.NOT_SCANNING;
                    }
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mFoundDevice = null;
            mBleScanState = BleScanState.SCANNING;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mBleScanState = BleScanState.NOT_SCANNING;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // String readerName = "PiAdvertiserECE";
                    String readerName = "OnePlus 3T";
                    // TODO: Potentially use BluetoothAdapter.getRemoteDevice() instead?

                    // Look for specific device, and automatically connect to it.
                    if (device != null && device.getName() != null) {
                        Log.d(tag, "found device=" + device.getName());
                        if (device.getName().equals(readerName)) {
                            mFoundDevice = device;
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                            mBleScanState = BleScanState.CONNECTED;
                            invalidateOptionsMenu();
                        }
                    }
                }
            });
        }
    };
}
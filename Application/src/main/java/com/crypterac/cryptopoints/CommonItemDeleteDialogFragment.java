package com.crypterac.cryptopoints;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ArrayAdapter;

/**
 * Created by peter on 20/04/18.
 */

public class CommonItemDeleteDialogFragment extends DialogFragment {
    private static final String TAG = CommonItemDeleteDialogFragment.class.getName();

    private DialogInterface.OnClickListener positiveListener;
    private DialogInterface.OnClickListener negativeListener;

    public void setup(DialogInterface.OnClickListener positiveListener,
                      DialogInterface.OnClickListener negativeListener) {
        this.positiveListener = positiveListener;
        this.negativeListener = negativeListener;
    }

    // @: https://developer.android.com/guide/topics/ui/dialogs.html
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete item?")
                .setPositiveButton("YES", positiveListener)
                .setNegativeButton("NO", negativeListener);
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
